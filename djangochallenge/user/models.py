from django.db import models
from django.core.urlresolvers import reverse

# Create your models here.
class User(models.Model) :
    full_name = models.CharField(max_length=100)
    email = models.EmailField()
    FEMALE = '0'
    MALE = '1'
    GENDER = ((FEMALE, 'Female'), (MALE, 'Male'))
    gender = models.CharField(max_length = 1, choices = GENDER, default = FEMALE)

    # Url to redirects to the list page after creating a User
    def get_absolute_url(self):
        return reverse('user:index')

    # def __str__(self):
    #     return self.full_name + ' ' + self.email