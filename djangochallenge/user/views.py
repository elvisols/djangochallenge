from django.views import generic
from django.shortcuts import render_to_response
from django.views.generic.edit import CreateView
from .models import User

# Create your home view
def home(request) :
    return render_to_response('user/home.html')

class IndexView(generic.ListView) :
    template_name = 'user/index.html'
    context_object_name = 'users' # default = 'object_list'
    def get_queryset(self):
        return User.objects.all()

class UserCreate(CreateView) :
    model = User # Model to create
    # Specify fields to add
    fields = ['full_name', 'email', 'gender']