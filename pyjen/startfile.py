import sqlite3
from jenkinsapi.jenkins import Jenkins
import datetime

# Jenkins instance path and login credential
jenkins_url = 'http://13.95.234.118:8080'
server = Jenkins(jenkins_url, username = 'jadmin', password = 'admin1234')

# print("Jenkins version: " + server.version)
# print(server.get_jobs_list())

# Sqlite connection string
conn = sqlite3.connect('mydb.db')
for j in server.get_jobs():
    job_instance = server.get_job(j[0])
    currentDateTime = datetime.datetime.now()
    now = datetime.datetime.strftime(currentDateTime, '%I:%M:%S%p - %d %b, %Y')
    # print('Job Description:%s' %(job_instance.get_description()))
    print("----------------- Adding to database ------------------")
    conn.execute("INSERT INTO joblogs (name, status, state, created) \
              VALUES (?,?,?,?)", [job_instance.name, job_instance.is_enabled(), job_instance.is_running(), now])
conn.commit()

print("----------------- Print Result ------------------")
job = conn.execute('select * from joblogs')
# index [0] = id field
response = [dict(name=row[1], status=row[2], state=row[3], created=row[4]) for row in job.fetchall()]
conn.close()
print(response)